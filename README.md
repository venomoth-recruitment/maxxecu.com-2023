# MaxxECU recruitment task

The repository contains solution for [MaxxECU](https://maxxecu.com/) recruitment task.

The task was to create MAUI application in .NET 6 displaying modern car gauge.

## Requirements

To build and run the project you need to open it in Visual Studio 2022.
