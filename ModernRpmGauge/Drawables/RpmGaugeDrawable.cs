﻿using Font = Microsoft.Maui.Graphics.Font;

namespace ModernRpmGauge.Drawables;

public class RpmGaugeDrawable : IDrawable
{
    public int RedlineRpm { get; set; } = 6500;
    public int RevLimitRpm { get; set; } = 7800;
    public int CurrentRpm { get; set; } = 7300;

    private readonly Color primaryGaugeColor = Colors.White;
    private readonly Color primaryGaugeDimColor = Colors.LightGrey;
    private readonly Color primaryGaugeRedlineColor = Colors.Red;
    private readonly Color primaryGaugeRedlineDimColor = Colors.DarkRed;
    private readonly Color primaryGaugeShadowColor = Colors.Aquamarine;
    private readonly SizeF primaryGaugeShadowOffset = new SizeF(0, -8);
    private readonly int primaryGaugeShadowBlur = 8;
    private readonly Color primaryGaugeRedlineShadowColor = Colors.Red;
    private readonly SizeF primaryGaugeRedlineShadowOffset = new SizeF(0, -8);
    private readonly int primaryGaugeRedlineShadowBlur = 8;
    private readonly int primaryGaugeSize = 8;
    private readonly PointF primaryGaugeArcOffset = new PointF(90, 30);

    private readonly Color secondaryGaugeColor = Colors.Aquamarine;
    private readonly Color secondaryGaugeDimColor = Colors.DarkOrange;
    private readonly int secondaryGaugeSize = 4;
    private readonly PointF secondaryGaugeStartAndEndPointOffset = new PointF(11, 4);
    private readonly PointF secondaryGaugeArcOffset = new PointF(79, 26);
    private readonly float[] secondaryGaugePattern = new float[] { 0.1f, 1.5f };

    private readonly Color numberColor = Colors.White;
    private readonly Color numberDimColor = Colors.LightGrey;
    private readonly Color numberRedlineColor = Colors.Red;
    private readonly Color numberRedlineDimColor = Colors.DarkRed;
    private readonly Color numberShadowColor = Colors.Aquamarine;
    private readonly Color numberRedlineShadowColor = Colors.Red;
    private readonly SizeF numberShadowOffset = new SizeF(0, 0);
    private readonly int numberShadowBlur = 8;
    private readonly Font numberFont = new Font("TahomaRegular", FontWeights.Heavy);
    private readonly int numberSize = 30;
    private readonly int numberDimSize = 26;
    private readonly PointF numberOffset = new PointF(0, 40);

    private int maxNumber;
    private int maxRpm;
    private float gaugeLength;

    private PointF primaryGaugeStartPoint;
    private PointF primaryGaugeEndPoint;
    private PointF primaryGaugeLeftArcPoint;
    private PointF primaryGaugeRightArcPoint;
    private PointF primaryGaugeRedlinePoint;

    private PointF secondaryGaugeStartPoint;
    private PointF secondaryGaugeEndPoint;
    private PointF secondaryGaugeLeftArcPoint;
    private PointF secondaryGaugeRightArcPoint;

    public void Draw(ICanvas canvas, RectF dirtyRect)
    {
        this.SetMaxNumberAndMaxRpm();
        this.SetGaugesPoints(dirtyRect);
        this.SetGaugeLength();
        this.SetPrimaryGaugeRedlinePoint();

        this.SetCanvas(canvas);
        this.DrawDimPrimaryGauge(canvas);
        this.DrawPrimaryGauge(canvas);
        this.DrawDimSecondaryGauge(canvas);
        this.DrawSecondaryGauge(canvas);
        this.DrawNumbers(canvas);
    }

    private void SetMaxNumberAndMaxRpm()
    {
        this.maxNumber = (int)Math.Ceiling((double)this.RevLimitRpm / 1000);
        this.maxRpm = this.maxNumber * 1000;
    }

    private void SetGaugesPoints(RectF dirtyRect)
    {
        this.primaryGaugeStartPoint = new PointF(dirtyRect.Left, dirtyRect.Center.Y);
        this.primaryGaugeEndPoint = new PointF(dirtyRect.Right, dirtyRect.Center.Y);
        this.primaryGaugeLeftArcPoint = new PointF(this.primaryGaugeStartPoint.X + this.primaryGaugeArcOffset.X, this.primaryGaugeStartPoint.Y - this.primaryGaugeArcOffset.Y);
        this.primaryGaugeRightArcPoint = new PointF(this.primaryGaugeEndPoint.X - this.primaryGaugeArcOffset.X, this.primaryGaugeEndPoint.Y - this.primaryGaugeArcOffset.Y);

        this.secondaryGaugeStartPoint = new PointF(dirtyRect.Left + this.secondaryGaugeStartAndEndPointOffset.X, dirtyRect.Center.Y + this.secondaryGaugeStartAndEndPointOffset.Y);
        this.secondaryGaugeEndPoint = new PointF(dirtyRect.Right - this.secondaryGaugeStartAndEndPointOffset.X, dirtyRect.Center.Y + this.secondaryGaugeStartAndEndPointOffset.Y);
        this.secondaryGaugeLeftArcPoint = new PointF(this.secondaryGaugeStartPoint.X + this.secondaryGaugeArcOffset.X, this.secondaryGaugeStartPoint.Y - this.secondaryGaugeArcOffset.Y);
        this.secondaryGaugeRightArcPoint = new PointF(this.secondaryGaugeEndPoint.X - this.secondaryGaugeArcOffset.X, this.secondaryGaugeEndPoint.Y - this.secondaryGaugeArcOffset.Y);
    }

    private void SetPrimaryGaugeRedlinePoint()
    {
        this.primaryGaugeRedlinePoint = new PointF(this.primaryGaugeLeftArcPoint.X + this.GetGaugeOffsetForRpm(this.RedlineRpm), this.primaryGaugeLeftArcPoint.Y);
    }

    private void SetGaugeLength()
    {
        this.gaugeLength = this.primaryGaugeRightArcPoint.X - this.primaryGaugeLeftArcPoint.X;
    }

    private void SetCanvas(ICanvas canvas)
    {
        canvas.StrokeLineJoin = LineJoin.Round;
        canvas.Font = numberFont;
    }

    private float GetGaugeOffsetForRpm(int rpm)
    {
        return this.gaugeLength * ((float)rpm / (float)this.maxRpm);
    }

    private void DrawPrimaryGauge(ICanvas canvas)
    {
        var endPoint = new PointF(this.primaryGaugeLeftArcPoint.X + this.GetGaugeOffsetForRpm(this.CurrentRpm), this.primaryGaugeLeftArcPoint.Y);

        if (endPoint.X > this.primaryGaugeRedlinePoint.X)
        {
            endPoint.X = this.primaryGaugeRedlinePoint.X;
        }

        canvas.StrokeLineCap = LineCap.Square;
        canvas.StrokeColor = this.primaryGaugeColor;
        canvas.StrokeSize = this.primaryGaugeSize;
        canvas.SetShadow(this.primaryGaugeShadowOffset, this.primaryGaugeShadowBlur, this.primaryGaugeShadowColor);

        var path = new PathF();
        path.MoveTo(this.primaryGaugeStartPoint);
        path.LineTo(this.primaryGaugeLeftArcPoint);
        path.LineTo(endPoint);

        canvas.DrawPath(path);

        canvas.SetShadow(new SizeF(0, 0), 0, null);

        this.DrawPrimaryGaugeRedline(canvas);
    }

    private void DrawPrimaryGaugeRedline(ICanvas canvas)
    {
        var endPoint = new PointF(this.primaryGaugeLeftArcPoint.X + this.GetGaugeOffsetForRpm(this.CurrentRpm), this.primaryGaugeLeftArcPoint.Y);

        if (endPoint.X < this.primaryGaugeRedlinePoint.X)
        {
            return;
        }

        canvas.StrokeLineCap = LineCap.Square;
        canvas.StrokeColor = this.primaryGaugeRedlineColor;
        canvas.StrokeSize = this.primaryGaugeSize;
        canvas.SetShadow(this.primaryGaugeRedlineShadowOffset, this.primaryGaugeRedlineShadowBlur, this.primaryGaugeRedlineShadowColor);

        var path = new PathF();
        path.MoveTo(this.primaryGaugeRedlinePoint);
        path.LineTo(endPoint);

        canvas.DrawPath(path);

        canvas.SetShadow(new SizeF(0, 0), 0, null);
    }

    private void DrawDimPrimaryGauge(ICanvas canvas)
    {
        canvas.StrokeLineCap = LineCap.Square;
        canvas.StrokeColor = this.primaryGaugeDimColor;
        canvas.StrokeSize = this.primaryGaugeSize;

        var path = new PathF();
        path.MoveTo(this.primaryGaugeStartPoint);
        path.LineTo(this.primaryGaugeLeftArcPoint);
        path.LineTo(this.primaryGaugeRightArcPoint);
        path.LineTo(this.primaryGaugeEndPoint);

        canvas.DrawPath(path);

        this.DrawDimPrimaryGaugeRedline(canvas);
    }

    private void DrawDimPrimaryGaugeRedline(ICanvas canvas)
    {
        canvas.StrokeLineCap = LineCap.Square;
        canvas.StrokeColor = this.primaryGaugeRedlineDimColor;
        canvas.StrokeSize = this.primaryGaugeSize;

        var path = new PathF();
        path.MoveTo(this.primaryGaugeRedlinePoint);
        path.LineTo(this.primaryGaugeRightArcPoint);
        path.LineTo(this.primaryGaugeEndPoint);

        canvas.DrawPath(path);
    }

    private void DrawSecondaryGauge(ICanvas canvas)
    {
        var endPoint = new PointF(this.secondaryGaugeLeftArcPoint.X + this.GetGaugeOffsetForRpm(this.CurrentRpm), this.secondaryGaugeLeftArcPoint.Y);

        canvas.StrokeLineCap = LineCap.Round;
        canvas.StrokeColor = this.secondaryGaugeColor;
        canvas.StrokeSize = this.secondaryGaugeSize;
        canvas.StrokeDashPattern = this.secondaryGaugePattern;

        var path = new PathF();
        path.MoveTo(this.secondaryGaugeStartPoint);
        path.LineTo(this.secondaryGaugeLeftArcPoint);
        path.LineTo(endPoint);

        canvas.DrawPath(path);

        canvas.StrokeDashPattern = null;
    }

    private void DrawDimSecondaryGauge(ICanvas canvas)
    {
        canvas.StrokeLineCap = LineCap.Round;
        canvas.StrokeColor = this.secondaryGaugeDimColor;
        canvas.StrokeSize = this.secondaryGaugeSize;
        canvas.StrokeDashPattern = this.secondaryGaugePattern;

        var path = new PathF();
        path.MoveTo(this.secondaryGaugeStartPoint);
        path.LineTo(this.secondaryGaugeLeftArcPoint);
        path.LineTo(this.secondaryGaugeRightArcPoint);
        path.LineTo(this.secondaryGaugeEndPoint);

        canvas.DrawPath(path);

        canvas.StrokeDashPattern = null;
    }

    private void DrawNumbers(ICanvas canvas)
    {
        for (int number = 0; number <= this.maxNumber; number++)
        {
            if (this.IsNumberDimForRpm(number, this.CurrentRpm))
            {
                this.DrawDimNumber(canvas, number);
            }
            else
            {
                this.DrawNumber(canvas, number);
            }
        }
    }

    private void DrawNumber(ICanvas canvas, int number)
    {
        var isNumberOnRedline = this.IsNumberOnRedline(number);

        canvas.FontSize = this.numberSize;
        canvas.FontColor = isNumberOnRedline ? this.numberRedlineColor : this.numberColor;
        canvas.SetShadow(this.numberShadowOffset, this.numberShadowBlur, isNumberOnRedline ? this.numberRedlineShadowColor : this.numberShadowColor);

        canvas.DrawString(
            number.ToString(),
            this.primaryGaugeLeftArcPoint.X + this.numberOffset.X + ((float)number / (float)this.maxNumber) * this.gaugeLength,
            this.primaryGaugeLeftArcPoint.Y + this.numberOffset.Y,
            HorizontalAlignment.Center
        );

        canvas.SetShadow(new SizeF(0, 0), 0, null);
    }

    private void DrawDimNumber(ICanvas canvas, int number)
    {
        canvas.FontSize = this.numberDimSize;
        canvas.FontColor = this.IsNumberOnRedline(number) ? this.numberRedlineDimColor : this.numberDimColor;

        canvas.DrawString(
            number.ToString(),
            this.primaryGaugeLeftArcPoint.X + this.numberOffset.X + ((float)number / (float)this.maxNumber) * this.gaugeLength,
            this.primaryGaugeLeftArcPoint.Y + this.numberOffset.Y,
            HorizontalAlignment.Center
        );
    }

    private bool IsNumberDimForRpm(int number, int rpm)
    {
        return rpm < number * 1000;
    }

    private bool IsNumberOnRedline(int number)
    {
        return this.RedlineRpm <= number * 1000;
    }
}