﻿namespace ModernRpmGauge;

public static class MauiProgram
{
	public static MauiApp CreateMauiApp()
	{
		var builder = MauiApp.CreateBuilder();
		builder
			.UseMauiApp<App>()
			.ConfigureFonts(fonts =>
			{
				fonts.AddFont("Tahoma-Regular.ttf", "TahomaRegular");
				fonts.AddFont("Tahoma-Bold.ttf", "TahomaBold");
			});

		return builder.Build();
	}
}
