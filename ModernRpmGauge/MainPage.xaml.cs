﻿using ModernRpmGauge.Drawables;
using System.Timers;

namespace ModernRpmGauge;

public partial class MainPage : ContentPage
{
    private readonly int idleRpm = 780;
    private readonly int revLimitRpm = 7800;
    private readonly int revUpStep = 200;
    private readonly int revDownStep = 175;
    private readonly int revLimitTreshold = 300;
    private readonly int refreshTimeInMiliseconds = 50;

    private bool isRevUpPressed = false;
    private bool isRevLimitReached = false;

    public MainPage()
    {
        InitializeComponent();

        var rpmGaugeView = this.RpmGaugeView;
        var rpmGaugeDrawable = (RpmGaugeDrawable)rpmGaugeView.Drawable;
        rpmGaugeDrawable.RevLimitRpm = this.revLimitRpm;
        rpmGaugeDrawable.CurrentRpm = this.idleRpm;

        var timer = new System.Timers.Timer(this.refreshTimeInMiliseconds);
        timer.Elapsed += new ElapsedEventHandler(this.UpdateGauge);
        timer.Start();
    }

    private void UpdateGauge(object sender, EventArgs e)
    {
        var rpmGaugeView = this.RpmGaugeView;
        var rpmGaugeDrawable = (RpmGaugeDrawable)rpmGaugeView.Drawable;

        rpmGaugeDrawable.CurrentRpm = this.CalculateNextRpm(rpmGaugeDrawable.CurrentRpm);
        rpmGaugeView.Invalidate();
    }

    private int CalculateNextRpm(int currentRpm)
    {
        if (this.isRevUpPressed && !this.isRevLimitReached)
        {
            var nextRpm = currentRpm + this.revUpStep;

            if (nextRpm >= this.revLimitRpm)
            {
                nextRpm = this.revLimitRpm;
                this.isRevLimitReached = true;
            }

            return nextRpm;
        }
        else
        {
            var nextRpm = currentRpm - this.revDownStep;

            if (nextRpm < this.idleRpm)
            {
                nextRpm = this.idleRpm;
            }

            if (this.isRevLimitReached && nextRpm < this.revLimitRpm - this.revLimitTreshold)
            {
                this.isRevLimitReached = false;
            }

            return nextRpm;
        }
    }

    private void OnRevUpPressed(object sender, EventArgs e)
    {
        this.isRevUpPressed = true;
    }

    private void OnRevUpReleased(object sender, EventArgs e)
    {
        this.isRevUpPressed = false;
    }
}

